#include <windows.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "qoidecode.h"

// window
WNDCLASS wc;
MSG msg;
HWND hwnd;
RECT window_rect;
RECT img_pos;

// frame
uint32_t* frame_buf;
BITMAPINFO frame_bmpinfo;
HBITMAP frame_bmp;
HDC frame_dc;
int width, height;
int center_x, center_y;
int offset_x = 0;
int offset_y = 0;

// QOI file
FILE* fp;

void
invalid_qoi(char* fn)
{
		fclose(fp);
		MessageBox(NULL, "Not a valid QOI image", fn, 16);
		exit(1);
}

void
cannot_open(char* fn)
{
		MessageBox(NULL, "Unable to open file for reading", fn, 16);
		exit(1);
}

void
get_cmdline_arg(char* dest, char* src)
{
	int src_i = 0, dest_i = 0;
	char end_char = ' ';

	int cmdlinelen = strlen(src);

	// Is the first argument in quotes?
	if (src[0] == '"') {
		src_i++;
		end_char = '"';
	}

	// copy first argument to dest string
	while (src_i < cmdlinelen && dest_i < MAX_PATH) {
		// copy literal exit character indicated by backslash
		if (src[src_i] == '\\' && src[src_i+1] == end_char) {
			dest[dest_i] = end_char;
			src_i += 2; dest_i++;
			continue;
		}

		// break at end of first argument
		if (src[src_i] == end_char) break;

		// copy character
		dest[dest_i] = src[src_i];
		src_i++; dest_i++;
	}
}

LRESULT CALLBACK
window_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT paint;
	HDC dc;

	switch (uMsg) {
	case WM_CREATE:
		break;
	case WM_DESTROY:
	case WM_QUIT:
	case WM_CLOSE:
		exit(0);
	case WM_PAINT: // draw bitmap
		dc = BeginPaint(hwnd, &paint);
		GetClientRect(hwnd, &window_rect);

		// calculate image position
		img_pos.left = (window_rect.right / 2) - center_x + offset_x;
		img_pos.top = (window_rect.bottom / 2) - center_y + offset_y;
		img_pos.right = img_pos.left + width;
		img_pos.bottom = img_pos.top + height;

		// set brush to draw background
		SelectObject(dc, GetStockObject(DC_BRUSH));
		SetDCBrushColor(dc, 0x000000);
		SetDCPenColor(dc, 0x000000);

		// background around image
		Rectangle(dc, -1, 0, img_pos.left, window_rect.bottom); // left
		Rectangle(dc, 0, -1, window_rect.right, img_pos.top); // top
		Rectangle(dc, img_pos.right, 0, window_rect.right, window_rect.bottom); // right
		Rectangle(dc, 0, img_pos.bottom, window_rect.right, window_rect.bottom); // bottom

		// draw image
		BitBlt(
				dc,	// destination DC
				img_pos.left, // destination left
				img_pos.top, // destination top
				width, // width
				height, // height
				frame_dc, // source DC
				0, // source left
				0, // source top
				SRCCOPY // method
		);

		EndPaint(hwnd, &paint);
		break;
	case WM_SIZE:
		InvalidateRect(hwnd, NULL, 0);
		UpdateWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_UP: // move image with arrow keys
			offset_y += 50;
			break;
		case VK_DOWN:
			offset_y -= 50;
			break;
		case VK_LEFT:
			offset_x += 50;
			break;
		case VK_RIGHT:
			offset_x -= 50;
			break;
		case 0x43: // 'C' key, center image
			offset_x = 0;
			offset_y = 0;
			break;
		case 0x51: // 'Q' key, quit
		case VK_ESCAPE:
			exit(0);
		}
		InvalidateRect(hwnd, NULL, 0);
		UpdateWindow(hwnd);
		break;
	}

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

int WINAPI
WinMain(
		HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPSTR lpCmdLine,
		int nCmdShow
) {
	char fn[MAX_PATH] = {0};

	// get QOI file path
	if (strlen(lpCmdLine) == 0) {
		// get QOI file path with open dialog
		OPENFILENAME ofn = {
			sizeof(OPENFILENAME),
			NULL,
			hInstance,
			"Quite OK Image (*.qoi)\0*.QOI\0\0",
			NULL,
			0,
			1,
			fn,
			MAX_PATH,
			NULL,
			0,
			NULL,
			NULL,
			OFN_FILEMUSTEXIST | OFN_NONETWORKBUTTON | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY,
			0,
			0,
			"qoi",
			(LPARAM)NULL,
			NULL, NULL, NULL, 0, 0
		};
		if (!GetOpenFileNameA(&ofn)) return 0;
	} else {
		// get QOI file path from command line
		get_cmdline_arg(fn, lpCmdLine);
	}

	// open QOI file
	fp = fopen(fn, "rb");
	if (fp == NULL) cannot_open(fn);

	if (!load_qoi_header(fp, &frame_bmpinfo)) invalid_qoi(fn);
	width = frame_bmpinfo.bmiHeader.biWidth;
	height = frame_bmpinfo.bmiHeader.biHeight * -1;
	center_x = width / 2;
	center_y = height / 2;

	// load QOI into bitmap
	frame_bmp = CreateDIBSection(
			NULL,
			&frame_bmpinfo,
			DIB_RGB_COLORS,
			(void**)&frame_buf,
			0, 0
	);
	if (!decode_qoi(
			fp,
			frame_buf,
			width,
			height
	)) invalid_qoi(fn);

	// after it has loaded
	fclose(fp);
	frame_dc = CreateCompatibleDC(NULL);
	SelectObject(frame_dc, frame_bmp);

	// register window class
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = NULL;
	wc.hCursor = LoadCursor(hInstance, IDC_ARROW);
	wc.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = window_proc;
	wc.lpszClassName = "qoiwinthingy";
	wc.lpszMenuName = NULL;
	wc.style = CS_VREDRAW | CS_HREDRAW;

	if (!RegisterClass(&wc)) {
		MessageBox(NULL, "Could not register window class", "Fatal Error", 16);
		return 1;
	}

	// window border
	window_rect.right = width;
	window_rect.bottom = height;
	AdjustWindowRectEx(
			&window_rect,
			WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			0, 0
	);

	// create window
	hwnd = CreateWindowEx(0,
		wc.lpszClassName,
		fn,						// window title
		WS_OVERLAPPEDWINDOW | WS_VISIBLE,	// window style
		CW_USEDEFAULT,
		CW_USEDEFAULT,		// window position
		window_rect.right - window_rect.left,	// window size
		window_rect.bottom - window_rect.top,
		NULL, NULL, hInstance, NULL
	);
	if (hwnd == NULL) {
		MessageBox(NULL, "Could not create window", "Fatal Error", 16);
		return 1;
	}

	// invalidate window for drawing
	InvalidateRect(hwnd, NULL, 0);
	UpdateWindow(hwnd);

	// message loop
	for (;;) {
		while (GetMessage(&msg, NULL, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return 0;
}
