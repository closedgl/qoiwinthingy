all: qwt.exe

qwt.exe: main.c qoidecode.c
	i686-w64-mingw32-gcc main.c qoidecode.c -mwindows -o qwt.exe

x86_64: main.c qoidecode.c
	x86_64-w64-mingw32-gcc main.c qoidecode.c -mwindows -o qwt-x64.exe
