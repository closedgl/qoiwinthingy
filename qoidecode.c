#include <windows.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "qoidecode.h"

uint8_t
load_qoi_header(FILE* fp, BITMAPINFO* bmpinfo)
{
	BITMAPINFOHEADER* bmiHeader = &bmpinfo->bmiHeader;

	bmiHeader->biSize = sizeof(BITMAPINFOHEADER);
	bmiHeader->biPlanes = 1;
	bmiHeader->biBitCount = 32;
	bmiHeader->biCompression = BI_RGB;

	// read magic
	char magic[5] = {0};
	fread(magic, 4, 1, fp);
	if (strcmp(magic, "qoif") != 0) return 0;

	// read width
	// little-endian yayyyyyyy
	bmiHeader->biWidth =
		(fgetc(fp) << 24) |
		(fgetc(fp) << 16) |
		(fgetc(fp) <<  8) |
		fgetc(fp);

	// read height
	bmiHeader->biHeight =
		((fgetc(fp) << 24) |
		(fgetc(fp) << 16) |
		(fgetc(fp) <<  8) |
		fgetc(fp)) * -1;

	// seek past channels and colorspace
	fseek(fp, 2, SEEK_CUR);

	return 1;
}

uint8_t
decode_qoi(FILE* fp, void* buf, int width, int height)
{
	unsigned int total_pixels;
	unsigned int index = 0;

	Pixel* buffer = buf;

	Pixel pixel;
	pixel.r = 0;
	pixel.g = 0;
	pixel.b = 0;
	pixel.a = 255;

	Pixel array[64] = {0};
	uint8_t index_position;

	uint8_t byte;
	uint8_t run = 1;
	uint8_t dg;

	total_pixels = width * height;

	// decode image
	while (index < total_pixels) {
		// read byte
		fread(&byte, 1, 1, fp);

		// decode tag
		switch (byte & 0xc0) {
		case 0xc0: // could be run, rgb, or rgba
			switch (byte) {
			case QOI_OP_RGB: // RGB pixel value
				fread(&pixel.r, 1, 1, fp);
				fread(&pixel.g, 1, 1, fp);
				fread(&pixel.b, 1, 1, fp);
				break;
			case QOI_OP_RGBA: // RGBA pixel value
				fread(&pixel.r, 1, 1, fp);
				fread(&pixel.g, 1, 1, fp);
				fread(&pixel.b, 1, 1, fp);
				fread(&pixel.a, 1, 1, fp);
				break;
			default: // Run-length encoding
				run = (byte & 0x3f) + 1;
				break;
			}
			break;
		case QOI_OP_INDEX: // index previously seen color
			pixel = array[byte];
			break;
		case QOI_OP_DIFF: // small difference from last pixel
			pixel.r += (((byte & 0x30) >> 4) - 2);
			pixel.g += (((byte & 0x0c) >> 2) - 2);
			pixel.b += ((byte & 0x03) - 2);
			break;
		case QOI_OP_LUMA: // large difference from last pixel
			dg = (byte & 0x3f) - 32;
			pixel.g += dg;
			fread(&byte, 1, 1, fp);
			pixel.r += ((((byte & 0xf0) >> 4) - 8) + dg);
			pixel.b += (((byte & 0x0f) - 8) + dg);
			break;
		}

		// draw pixel
		for (;run > 0;run--) {
			buffer[index] = pixel;
			index++;
		}
		run = 1;

		// add pixel to array
		index_position = ((pixel.r * 3) + (pixel.g * 5) + (pixel.b * 7) + (pixel.a * 11)) % 64;
		array[index_position] = pixel;
	}

	// check for end magic
	for (run = 0; run < 7; run++) {
		fread(&byte, 1, 1, fp);
		if (byte != 0) return 0;
	}
	fread(&byte, 1, 1, fp);
	if (byte != 1) return 0;

	return 1;
}
