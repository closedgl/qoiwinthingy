#ifndef qoidecode_h
#define qoidecode_h

#include <windows.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

uint8_t load_qoi_header(FILE* fp, BITMAPINFO* bmpinfo);
uint8_t decode_qoi(FILE* fp, void* buf, int width, int height);

typedef struct {
	char magic[4];
	uint32_t width;
	uint32_t height;
	uint8_t channels;
	uint8_t colorspace;
} QOIHeader;

typedef struct {
	uint8_t b, g, r, a;
} Pixel;

#define QOI_OP_RGB		0xfe
#define QOI_OP_RGBA		0xff
#define QOI_OP_INDEX	0x00
#define QOI_OP_DIFF		0x40
#define QOI_OP_LUMA		0x80
#define QOI_OP_RUN		0xc0

#endif // qoidecode_h
