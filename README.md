# qoiwinthingy

My attempt at decoding [QOI images](https://qoiformat.org) into Windows bitmaps

![dice.qoi example in qoiwinthingy](doc/screenshot.png)

## Building

1. Make sure MinGW is installed on your system
2. Run `make` for 32-bit Windows, or `make x86_64` for 64-bit Windows
